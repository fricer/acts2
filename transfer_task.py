import sys
import torch
import nltk
import argparse

import numpy as np

from torchtext.data import Field
from torchtext.vocab import GloVe

PATH_TO_SENTEVAL = "./SentEval"
sys.path.insert(0, PATH_TO_SENTEVAL)
import senteval

from train import load_model

def prepare(params, samples):
    args = params.args

    text = Field(tokenize=nltk.word_tokenize, lower=True)

    text.build_vocab(samples, vectors=GloVe(name=args.web_craw, dim=args.w_embed_dims))

    encoder = load_model(args.checkpoint, args.device).get_encoder()

    encoder.set_embed_layer(text.vocab)

    encoder.to(args.device)

    params['encoder'] = encoder

def batcher(params, batch):

    lens = [len(sent) if sent != [] else 1 for sent in batch]
    
    max_len = max(lens)

    for i, sent in enumerate(batch):
        new_sent = []

        if sent == []:
            new_sent.append(params['encoder'].vocab.stoi['<pad>'])
        else:
            for word in sent:
                new_sent.append(params['encoder'].vocab.stoi[word])

        batch[i] = new_sent + (max_len - len(new_sent)) * [1]

    return params['encoder'](
                                torch.tensor(batch).to(params.args.device), 
                                torch.tensor(lens).to(params.args.device)
                            ).cpu().detach()


def micro_macro_avg(results_dict):
    
    accs = []
    n_samples = []

    for task_dict in results_dict.values():
        if "devacc" not in task_dict.keys() or "ndev" not in task_dict.keys():
            continue

        accs.append(task_dict["devacc"])
        n_samples.append(task_dict["ndev"])

    accs = np.asarray(accs)
    n_samples = np.asarray(n_samples)

    return np.sum(np.multiply(accs, n_samples)) / sum(n_samples), np.mean(accs)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()

    parser.add_argument("--w_embed_dims", type=int, default=300, help="Word Embeddings dimensionality")
    parser.add_argument("--checkpoint", type=str, default=None, help="Checkpoint to resume training from", required=True)
    parser.add_argument("--web_craw", type=str, default="6B", help="Web Craw number of words")
    parser.add_argument("--data_path", type=str, default="./SentEval/data", help="Path to SentEval data")
    parser.add_argument("--n_folds", type=int, default=5, help="Number of folds")

    parser.add_argument("--nhid", type=int, default=0, help="MLP type for transfer learning task")
    parser.add_argument("--optim", type=str, default='rmsprop', help="Optimizer")
    parser.add_argument("--batch_size", type=int, default=128, help="Batch size")
    parser.add_argument("--epoch_size", type=int, default=2, help="Epoch size")
    
    args = parser.parse_args()

    args.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    params_senteval = {'task_path': args.data_path, 'usepytorch': True, 'kfold': args.n_folds}
    params_senteval['classifier'] = {'nhid': args.nhid, 'optim': args.optim, 'batch_size': args.batch_size, 'tenacity': 3, 'epoch_size': args.epoch_size}
    params_senteval['args'] = args

    se = senteval.engine.SE(params_senteval, batcher, prepare)
    transfer_tasks = ['MR', 'CR', 'SUBJ', 'MPQA', 'SST2', 'TREC', 'MRPC',
                      'SICKEntailment', 'SICKRelatedness', 'STS14']

    results = se.eval(transfer_tasks)

    print(results)

    micro, macro = micro_macro_avg(results)

    print(micro, macro)