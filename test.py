import argparse
import nltk

import torch

from train import load_split_data, load_model, eval_model

import pdb

def sentence_to_indx(vocab, sent):
    sent = nltk.word_tokenize(sent)

    idxs = []
    for word in sent:
        idxs.append(vocab.stoi[word.lower()])
    
    return idxs 

def add_pad(batch, max_len):

    for i, sent in enumerate(batch):

        batch[i] = batch[i] + (max_len - len(batch[i]))*[1]

    return batch

def make_batch(vocab, examples):

    premises = []
    hypoths = []

    lens_p = []
    lens_h = []

    for example in examples:
        prem = sentence_to_indx(vocab, example[0])
        hypo = sentence_to_indx(vocab, example[1])

        premises.append(prem)
        lens_p.append(len(prem))

        hypoths.append(hypo)
        lens_h.append(len(hypo))

    premises = add_pad(premises, max(lens_p))
    hypoths = add_pad(hypoths, max(lens_h))

    return (premises, lens_p), (hypoths, lens_h)

def main(args):

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    (text, label), (_, _, iter_test) = load_split_data(
                                                    nltk.word_tokenize, 
                                                    args.web_craw, 
                                                    args.w_embed_dims, 
                                                    args.batch_size, 
                                                    device
                                                )

    model = load_model(args.checkpoint, device)

    # pdb.set_trace()

    if args.eval_test:
        accuracy = eval_model(model, iter_test)

        print(accuracy)
    else:

        with open(args.query_file, 'r') as f:
            examples = f.readlines()

        examples = [example.rstrip().split(';') for example in examples]

        premise, hypoth = make_batch(text.vocab, examples)

        premise = torch.tensor(premise[0]).to(device), torch.tensor(premise[1]).to(device)
        hypoth = torch.tensor(hypoth[0]).to(device), torch.tensor(hypoth[1]).to(device)

        result = torch.argmax(model(premise, hypoth), dim=1).cpu().numpy()

        for i in result:
            print(label.vocab.itos[i])


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--batch_size", type=int, default=64, help="Batch size for BucketIterator")
    parser.add_argument("--w_embed_dims", type=int, default=300, help="Embeddings dimensionality")
    parser.add_argument("--web_craw", type=str, default="6B", help="Web Craw number of words")

    parser.add_argument("--checkpoint", type=str, help="Checkpoint to resume training from", required=True)
    parser.add_argument("--query_file", type=str, default="./examples.txt", help="File containing sentence examples")
    parser.add_argument("--eval_test", type=lambda x: True if x.lower() == "true" else False, default=False, help="Apply test set to model")

    args = parser.parse_args()
    main(args)