# General-purpose sentence representations in the natural language inference (NLI) task

Ivan Yovchev 12871737

## Setup guide

In order to install all of the packages required to run the code the simplest
way is to use [_conda_](https://docs.conda.io/en/latest/miniconda.html)

To create the environment:
```
conda env create -f env.yml
```
To then use the created conda environment:
```
conda activate atcs
```

For the transfer learning task first navigate to the main project directory using _cd_, then
clone the repo:
```
git clone https://github.com/facebookresearch/SentEval.git
cd SentEval/
```
To install SentEval simply do:
```
python setup.py install
```
Finally, download the datasets (requires _curl_):
```
cd data/downstream/
./get_transfer_data.bash
```

If executing ```./get_transfer_data.bash``` throws a declare error open the file with any
text aditor and add the following shebang as the first line - ```#!/bin/bash```

All requirements should now be fullfiled to be able to execute the code.

## Code navigation

* ```model.py``` - contains class definitions for encoders and classifier
* ```train.py``` - script used for training the model
* ```test.py``` - script used for evaluating the model
* ```transfer_task``` - script to perform transfer learning task

## Model

The scirpt ```model.py``` contains the following class definitions

Class ```Encoder``` - this is the parent class to all encoders. It contains
a ```torch.nn.Embedding``` layer which converts word indexes to the correct
GloVe word embedding. The function ```set_embed_layer``` copies the pre-trained
embeddings to the embedding layer and freezes it by setting 
```layer.weight.requires_grad = False```. The ```Encoder``` class takes two
arguments in its constructor a ```vocab``` used to initialize the Embedding
layer and a computing ```device```.

Class ```MeanEncoder``` is a child class of ```Encoder```. The constructor of 
the class is the same as that of the parent. In the forward pass through the 
network the sum of all embedding over the time dimension is computed. Said sum
is then divided by the corresponding sequence length to produce the sentence
embeddings.

Class ```LstmEncoder``` is a child class of ```Encoder```. The constructor
takes several more parameters including word embedding size, sentence embedding
size, number of directions and a flag for max pooling. An LSTM is initialized
using said parameters. In the forward pass of the network the hidden states of
the LSTM are computed, then one of two things happen:

* If the results are not being pooled, a batch is returned according to the
sequence lengths
* If the results are being pooled, max pooling is applied to ```dim=1```

Class ```Classifier``` - this class contains two linear layers with ReLU
activation between them. The constructor takes as arguments an encoder,
the size of the embeddings, the size of the hidden layer and the number of
classes. Said parameters are used to initialize the layers. In the forward
pass the premise and hypothesis are encoded using the same encoder. And the
resulting embeddings are passed down to the linear layers to classify.

## Training

The general way to initiate the training procedure is the following:
```
python3 train.py --encoder=[mean|lstm|bilstm|bilstm_pool]
```

### Training parameters

The scirpt ```train.py``` takes the following arguments and their corresponding
default values:

* ```--batch_size``` - The batch size to be used during training for the 
```BucketIterator```. The default value is **64**.
* ```--w_embed_dims``` - The size of the word embeddings. Defaults to **300**.
* ```--s_embed_dims``` - The size of the sentence embeddings. Defaults to **2048**.
* ```--lr``` - The learning rate to be used in the training process for the Adam
optimizer, the default is **0.0005**.
* ```--n_classes``` - The number of output classes for the network. Given the
SNLI dataset the default here is **3**.
* ```--n_epochs``` - The number of epochs to apply the training procedure on, the
default here is **12**.
* ```--encoder``` - The type of encoder to be used for the sentence embeddings.
The available options are: ```mean```, ```lstm```, ```bilstm``` and 
```bilstm_pool```. The default here is **```mean```** producing sentence embeddings
of size ```w_embed_dims```, by simply averaging all of the word embeddings. If
```lstm``` is selected the encoder is an LSTM the last hidden state of which is
considered the sentence embedding (size ```s_embed_dims```). If ```bilstm``` is
selected a BiLSTM is used where the sentence embedding is the concatentation 
between the last hidden states of the forward and backward layers. The size of
the resulting sentence embeddins is 2 * ```s_embed_dims```. Finally, if
```bilstm_pool``` is selected a BiLSTM is used where the sentence embedding is
the concatenation between all hidden states in the forward and backward layers
with applied max pooling. The size of the sentence embeddings is as in the 
previous case 2 * ```s_embed_dims```.
* ```--checkpoint``` - Relative path to a saved state of the model from a
previous point in time, used to resume training. An example of using a
checkpoint - ```--checkpoint=./lstm/20200416-113654.cpt```. The default here is
**```None```** meaning the network is randomly initialized if a checkpoint is not 
specified.
* ```--web_craw``` - The size of the web craw to be used. The default here is
**```6B```** meaning the GloVe 6B web craw is used. The default is set a much
smaller value to make development easier. When training the model this
parameter **should be set to ```840B```**.

### Training process

At the begining of the script the active computing device is set to **cuda** if
available, otherwise the **CPU** is used. Next the SNLI data is loaded and
all the necessary data splits are created, namely - ```train```, ```dev``` and
```test```. Next the model is either loaded from a previous checkpoint or if no 
such checkpoint is provided the model is initialized randomly. Now training can
begin.

At each iteration of the trainin cycle:

1. The model is trained on the ```train``` split, computing the average loss
for that epoch
2. The model is evaluated on ```dev``` split, compuiting the accuracy for that
epoch
3. The model is saved to the corresponding direcotry named after the encoder
being used (if the directory does not exist it is created). Checkpoints names 
are saved in the format - YYYYMMDD-HHMMSS.

This process repeats ```n_epochs``` times. Finally, the test accuracy is
computed at the end of the training process.

## Evaluation

There are two general ways to evaluate the model.

Either on the ```test``` split by doing:
```
python3 test.py --checkpoint=path_to_checkpoint --eval_test=True
```

Or on specific examples written to a file:
```
python3 test.py --checkpoint=path_to_checkpoint --query_file=path_to_file
```
An example file is provided containing 4 entailment tasks which can be executed 
as follows:
```
python3 test.py --checkpoint=path_to_checkpoint --query_file=./examples.txt
```

### Evaluation parameters

The scirpt ```test.py``` takes the following arguments and their corresponding
default values:

* ```--batch_size``` - same as in **Training parameters**
* ```--w_embed_dims``` - same as in **Training parameters**
* ```--web_craw``` - same as in **Training parameters**
* ```--checkpoint``` - same as in **Training parameters**
* ```--query_file``` - a ```.txt``` file containing examples of entailment
tasks to be solved by the model. Each example must be on a new line, and the
premise and hypothesis within an example are seperated by a semi-colon(;).
An example is provided in the file ```examples.txt```. Said file is also the
default for this parameter
* ```--eval_test``` - boolean value indicating if the model should be evaluated
using the ```test``` data split. The default is **False**.

### Evaluation process

If the model is being evaluated on the ```test``` split the process is the same
as during the training procedure. The output of the scipt is the accuracy.

If the model is being evaluated on specific examples, said examples are first
tokenized, converted to embedding indexes, padded accordingly to length to form
a singular batch and only then passed to the model for classification. The 
output is the text labels for each of the examples, one on each new line.

## Transfer learning task

The scirpt ```transfer_task.py``` takes the following arguments and their corresponding
default values:

* ```--w_embed_dims``` - same as in **Training parameters**
* ```--checkpoint``` - same as in **Training parameters**
* ```--web_craw``` - same as in **Training parameters**
* ```--data_path``` - path to the SentEval tests. Default is ```./SentEval/data```
* ```--n_folds``` - number of k-folds. Default is **5**.
* ```--nhid``` - number of hidden layers for clssifier, default is **0**.
* ```--optim``` - Optimizer to use, default is **rmsprop**.
* ```--batch_size``` - batch size to use, default is **128**
* ```--epoch_size``` - number of epochs to train on, default is **2**.

### Output

The script prints the json from the test along side with the micro and macro
scores on the dev sets.

