import torch
import torchtext
import nltk

import argparse
import os
import time

from torchtext.data import Field, BucketIterator
from torchtext.vocab import GloVe
from torchtext.datasets import SNLI

from model import Classifier, MeanEncoder, LstmEncoder


# easer load of model applicable across other scripts too
def load_model(path, device):
    return torch.load(path, map_location=device)


def load_split_data(tokenizer, web_craw, w_embed_dims, batch_size, device):

    text = Field(
                tokenize=tokenizer, 
                lower=True, 
                include_lengths=True, # saves computations
                batch_first=True # easier logic over dims
            )
    # unk_token=None makes sure the labels are 0, 1, 2
    label = Field(sequential=False, unk_token=None, is_target=True)

    # get data splits
    train, val, test = SNLI.splits(text, label)

    # populate vocabs with vectors
    text.build_vocab(train, val, test, vectors=GloVe(name=web_craw, dim=w_embed_dims))
    label.build_vocab(train)

    # get data split iterators
    iter_train, iter_val, iter_test = BucketIterator.splits((train, val, test), batch_size=batch_size, device=device)

    return (text, label), (iter_train, iter_val, iter_test)


def train_model(model, loss, optim, batcher):
    model.train()
    # for shuffling
    batcher.init_epoch()

    avg_loss = 0
    for batch_count, batch in enumerate(batcher):

        optim.zero_grad()

        # forward pass throug model
        out = model(batch.premise, batch.hypothesis)
        
        # compute loss
        grad = loss(out, batch.label)

        # compute running average loss for epoch
        avg_loss = (avg_loss * batch_count + grad.item()) / (batch_count + 1)

        grad.backward()
        optim.step()

    return avg_loss


def eval_model(model, batcher):
    model.eval()
    batcher.init_epoch()

    correct = 0
    for batch_count, batch in enumerate(batcher):

        # forward pass
        out = model(batch.premise, batch.hypothesis)

        # get number of correct classifications
        correct += (torch.argmax(out, axis=1).view(-1) == batch.label).sum().item()
    
    # return accuracy
    return correct / len(batcher.dataset.examples)


def main(args):

    # get computing device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # get data splits iterators
    (text, label), (iter_train, iter_val, iter_test) = load_split_data(
                                                                        nltk.word_tokenize, 
                                                                        args.web_craw, 
                                                                        args.w_embed_dims, 
                                                                        args.batch_size, 
                                                                        device
                                                                    )

    # ensure all dirs exist for checkpoints to be saved 
    path = "./%s" % (args.encoder)
    if not os.path.exists(path):
        os.makedirs(path)

    # init model
    model = None
    if args.checkpoint != None:
        model = load_model("%s/%s" % (path, args.checkpoint), device)
    else:
        encoder = None
        if args.encoder == "mean":
            encoder = MeanEncoder(text.vocab, device)
            input_dim = args.w_embed_dims
        elif args.encoder == "lstm":
            encoder = LstmEncoder(text.vocab, args.w_embed_dims, args.s_embed_dims, 1, False, device)
            input_dim = args.s_embed_dims
        elif args.encoder == "bilstm":
            encoder = LstmEncoder(text.vocab, args.w_embed_dims, args.s_embed_dims, 2, False, device)
            input_dim = 2 * args.s_embed_dims
        elif args.encoder == "bilstm_pool":
            encoder = LstmEncoder(text.vocab, args.w_embed_dims, args.s_embed_dims, 2, True, device)
            input_dim = 2 * args.s_embed_dims

        assert(encoder != None)

        model = Classifier(encoder, 4 * input_dim, device)

    assert(model != None)
    
    loss = torch.nn.CrossEntropyLoss()
    optim = torch.optim.Adam(model.parameters(), lr=args.lr)

    best_accuracy = -1
    for epoch in range(args.n_epochs):

        print("Epoch: ", epoch)

        avg_loss = train_model(model, loss, optim, iter_train)
        current_acc = eval_model(model, iter_val)
        
        print("Avg loss: ", avg_loss)
        print("Acc: ", current_acc)

        # save checkpoint
        time_stamp = time.strftime("%Y%m%d-%H%M%S")
        save_name = "%s/%s.cpt" % (path, time_stamp)
        torch.save(model, save_name)

    test_acc = eval_model(model, iter_test)
    print("Test accuracy: ", test_acc)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--batch_size", type=int, default=64, help="Batch size for BucketIterator")
    parser.add_argument("--w_embed_dims", type=int, default=300, help="Word Embeddings dimensionality")
    parser.add_argument("--s_embed_dims", type=int, default=2048, help="Sentence Embeddings dimensionality")

    parser.add_argument("--lr", type=float, default=0.0005, help="Starting learning rate")
    parser.add_argument("--n_classes", type=int, default=3, help="Number of classes")
    parser.add_argument("--n_epochs", type=int, default=12, help="Number of epochs to train on")

    parser.add_argument("--encoder", type=str, default="mean", help="Encoder type: [mean, lstm, bilstm, bilstm_pool]")
    parser.add_argument("--checkpoint", type=str, default=None, help="Checkpoint to resume training from")
    parser.add_argument("--web_craw", type=str, default="6B", help="Web Craw number of words")

    args = parser.parse_args()

    main(args)