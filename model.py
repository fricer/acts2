import torch
import torch.nn as nn


class Encoder(nn.Module):
    """docstring for Encoder"""
    def __init__(self, vocab, device):
        super(Encoder, self).__init__()

        # set embedding layer
        self.set_embed_layer(vocab)

        # set computing device
        self.to(device)

    def set_embed_layer(self, vocab):

        # init embed layer by unpacking the size of the vecab vectors
        self.embed = torch.nn.Embedding(*vocab.vectors.shape)

        # copy over pre-trained word embeddings
        self.embed.weight.data.copy_(vocab.vectors)

        # freeze layer
        self.embed.weight.requires_grad = False

        # store vocab for easy access
        self.vocab = vocab

    def forward(self, sentences, lengths):
        pass

class LstmEncoder(Encoder):
    """docstring for Mean"""
    def __init__(self, vocab, w_embed_d, s_embed_d, n_directions, max_pool, device):
        super(LstmEncoder, self).__init__(vocab, device)
        self.max_pool = max_pool

        # init LSTM according to params
        self.lstm = nn.LSTM(
                            input_size=w_embed_d, 
                            hidden_size=s_embed_d, 
                            bidirectional=True if n_directions==2 else False,
                            batch_first=True
                        )

    def forward(self, sentences, lengths):

        # convert to B x T x D (300)
        input_ = self.embed(sentences)

        # result has shape B x T x D
        result, _ = self.lstm(input_)

        # pool over T dimension
        if self.max_pool:

            # ensure padded positions are not picked
            result[sentences == 1, :].add(-float("inf"))

            # max pool over T dimension
            return torch.max(result, dim=1).values

        # output shape will be B x D
        return result[torch.arange(input_.shape[0]), lengths - 1, :]

class MeanEncoder(Encoder):

    def forward(self, sentences, lengths):
        # convert to B x T x D (300) sum and average according to seq length
        return self.embed(sentences).sum(dim=1) / lengths.view(-1, 1)

class Classifier(nn.Module):
    """docstring for Classifier"""
    def __init__(
            self,
            encoder,  
            input_dim, 
            device,
            hidden_dim = 512,
            n_classes = 3
        ):
        super(Classifier, self).__init__()

        self.encoder = encoder
        self.device = device

        # input_dim -> 512 -> 3
        self.classifier = nn.Sequential(
                nn.Linear(input_dim, hidden_dim),
                nn.ReLU(),
                nn.Linear(hidden_dim, n_classes)
            )

        self.to(device)

    def get_encoder(self):
        return self.encoder

    def forward(self, premise, hypoth):
        # encode premise and hypothesis
        u = self.encoder(premise[0], premise[1])
        v = self.encoder(hypoth[0], hypoth[1])

        # concatenate embeddings together
        cat = torch.cat([u, v, torch.abs(u-v), u*v], 1)

        # pass to classifer
        return self.classifier(cat)


